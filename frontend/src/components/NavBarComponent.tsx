﻿import React, {FunctionComponent, useContext} from "react";
import {Button, Nav, Navbar} from "react-bootstrap";
import {appContext} from "../contexts/AppContext";

const NavBarComponent : FunctionComponent = () =>{
    let {setShowStatus} = useContext(appContext);
    return(
        <div>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand>Todo app</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav" className = "justify-content-end">
                        <Button className = "float-right" size = "sm"  onClick = {()=> {
                            setShowStatus(true);
                        }}>Create new todo list</Button>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
};

export default NavBarComponent