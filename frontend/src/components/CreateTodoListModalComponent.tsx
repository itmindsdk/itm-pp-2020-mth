import React, {FunctionComponent, useCallback, useContext, useState} from "react";
import {Button, Form, Modal} from "react-bootstrap";
import {appContext} from "../contexts/AppContext";
import {CreateTodoListEntityCommand, TodoListEntityDto} from "../../NSwagTS/backend-api";
import {ListReducerActionType} from "../contexts/ListReducer";
import {clientContext} from "../contexts/ClientContext";


 
const CreateTodoListModalComponent : FunctionComponent = () =>{
    const [createTodoListCommand, setCreateTodoListCommand] = useState<CreateTodoListEntityCommand>(new CreateTodoListEntityCommand());
    const {todoListClient} = useContext(clientContext);
    let {showCreateTodoListModal,setShowStatus, todoListDispatcher} = useContext(appContext);
    
    const addTodoList = useCallback(
        (createTodoListCommand: CreateTodoListEntityCommand) => {
            todoListClient.create(createTodoListCommand).then(value => {
                todoListDispatcher({
                    type: ListReducerActionType.Add,
                    data: new TodoListEntityDto({id : value, name : createTodoListCommand.name})
                });
                setCreateTodoListCommand(new CreateTodoListEntityCommand({}));
                setShowStatus(false)
            });
           
        },
        [todoListDispatcher]
    );
    const handleChange = (e) =>{
        setCreateTodoListCommand(new CreateTodoListEntityCommand({name : e.target.value}));
    };
    
    return(
        <div>
            <Modal show={showCreateTodoListModal} onHide = {()=>{setShowStatus(false)}} >
                <Modal.Header closeButton>
                    <Modal.Title>Create new todo list</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="listName">
                            <Form.Label>List Name</Form.Label>
                            <Form.Control type="text" placeholder="Enter list name" onChange={handleChange} value={createTodoListCommand.name}/>
                        </Form.Group>
                        
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick = {()=>{setShowStatus(false)}}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={()=>addTodoList(createTodoListCommand)}>
                        Create todo list
                    </Button>
                </Modal.Footer>
            </Modal>

        </div>
    )
};

export default CreateTodoListModalComponent