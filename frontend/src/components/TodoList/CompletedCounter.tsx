﻿import React, {FunctionComponent} from "react";
import {TodoListItemEntityDto} from "../../../NSwagTS/backend-api";
import {Alert} from "react-bootstrap";
type Props = {
    todoItemList : TodoListItemEntityDto[] 
}
const CompletedCounterComponent : FunctionComponent<Props> = ({todoItemList})=>{
    const completedCount : number = todoItemList.reduce((count, item) => item.completed == true ? count + 1 : count, 0);
    const totalCount : number = todoItemList.length;
    function pickColor(){
        if(completedCount == totalCount){
            return 'success'
        }else{
            return 'danger'
        }
    }
    function generateCounter(){
        if(completedCount == 0 && totalCount == 0){
            return (<div/>)
        }else{
            return (<Alert style = {{"marginBottom" : "-1px"}}variant={pickColor()} > {completedCount} / {totalCount}</Alert>) 
        }
        
    }
    return (<div>{generateCounter()}</div>)
};

export default CompletedCounterComponent
