﻿import {
    CreateTodoListItemEntityCommand,
    TodoListItemEntityDto,
    TodoListWithItemsEntityDto, UpdateTodoListItemEntityCommand
} from "../../../NSwagTS/backend-api";
import {FunctionComponent, useCallback, useContext, useReducer, useState} from "react";
import ListReducer, {ListReducerActionType} from "../../contexts/ListReducer";
import {clientContext} from "../../contexts/ClientContext";
import {
    Button,
    ButtonGroup, ButtonToolbar,
    Card,
    Col,
    FormControl,
    InputGroup,
    ListGroup,
    Row,
    ToggleButton,
    ToggleButtonGroup
} from "react-bootstrap";
import CompletedCounterComponent from "./CompletedCounter";


type Props = {
    todoList : TodoListWithItemsEntityDto
}
function filterTodoItems(filter : string, todoItemList : TodoListItemEntityDto[]) : TodoListItemEntityDto[]{
    switch (filter) {
        case "done":
            return todoItemList.filter((item) => item.completed == true)
        case "unfinished":
            return todoItemList.filter((item) => item.completed != true)
        case "all":
            return todoItemList;
    }
    return [];
}

const TodoList : FunctionComponent<Props> = ({todoList}) =>{
    const [createTodoListItemCommand, setCreateTodoListItemCommand] = useState<CreateTodoListItemEntityCommand>(new CreateTodoListItemEntityCommand());
    const {todoListItemClient} = useContext(clientContext);
    const [todoItemList, todoItemListDispatcher] = useReducer(ListReducer<TodoListItemEntityDto>("itemId"), todoList.listItems);
    const addTodoItem = useCallback(
        (createTodoListItemEntityCommand: CreateTodoListItemEntityCommand) => {
            todoListItemClient.create(createTodoListItemEntityCommand, todoList.id.toString()).then(value => {
                todoItemListDispatcher({
                    type: ListReducerActionType.Add,
                    data: new TodoListItemEntityDto({completed : false, itemId : value, itemName : createTodoListItemEntityCommand.itemName})
                });
                setCreateTodoListItemCommand(new CreateTodoListItemEntityCommand({itemName:""}));
            });

        },
        [todoItemListDispatcher]
    );
    
    
    const deleteTodoItem = useCallback(
        (itemId:  number) => {
            todoListItemClient.delete(todoList.id, itemId).then(value => {
                todoItemListDispatcher({
                    type: ListReducerActionType.Remove,
                    data : itemId
                });
            });

        },
        [todoItemListDispatcher]
    );
    
    const markAsCompleted = useCallback((todoListItem : TodoListItemEntityDto) =>{
        todoListItemClient.update(todoList.id,todoListItem.itemId, new UpdateTodoListItemEntityCommand({
            itemId : todoListItem.itemId,
            itemName : todoListItem.itemName,
            completed : true,
            listId : todoList.id
        })).then(value => {
            todoItemListDispatcher({
                type : ListReducerActionType.Update,
                data : new TodoListItemEntityDto({
                    itemId : todoListItem.itemId,
                    itemName : todoListItem.itemName,
                    completed : true
                })
            })
        })
    }, [todoItemListDispatcher]);
    
    const [filter, setFilter] = useState("all");
    
    return(
            <Card style = {{"marginTop":"10px"}}>
                <Card.Header>{todoList.id} - {todoList.name}</Card.Header>
                <Card.Body>
                <ListGroup variant="flush">
                    <InputGroup className="mb-3" size = "sm">
                        <FormControl
                            placeholder="Todo item name.."
                            onChange={(e) =>{
                                setCreateTodoListItemCommand(new CreateTodoListItemEntityCommand({itemName : e.target.value, listId : todoList.id}));
                                console.log(createTodoListItemCommand);
                            }
                            }

                            value = {createTodoListItemCommand.itemName}/>
                        <InputGroup.Append>
                            <Button variant="primary" onClick = {()=>{addTodoItem(createTodoListItemCommand)}}>Add new todo item</Button>
                        </InputGroup.Append>
                    </InputGroup>
                    {filterTodoItems(filter, todoItemList).map(item =>
                    <ListGroup.Item key = {item.itemId}>
                        <Row>
                            <Col>
                                {item.itemId}-{item.itemName} - Completed: {item.completed.toString()}
                            </Col>
                            <Col >
                                {!item.completed && <Button className="float-right" variant="primary" onClick = {()=>{markAsCompleted(item)}}> Mark as completed</Button>}
                                
                                {item.completed == true ? <Button className = "float-right" variant = "danger" size = "sm" onClick={()=>{
                                    deleteTodoItem(item.itemId)
                                }}>Delete item</Button> :<Button size = "sm" style={{"marginLeft" : "5px"}} className="float-right" variant="primary" onClick = {()=>{markAsCompleted(item)}}> Mark as completed</Button>}
                                
                            </Col>
                        </Row>
                    </ListGroup.Item>)}
                    
                </ListGroup>
                </Card.Body>
                <Card.Footer>
                    <CompletedCounterComponent todoItemList={todoItemList}/>
                
                <Row><Col>
                    <ToggleButtonGroup type="radio" name="options" defaultValue={"all"} onChange = {(val) =>{
                        setFilter(val)
                    }}>
                    <ToggleButton value={"all"} size = "sm">All</ToggleButton>
                    <ToggleButton value={"done"} size = "sm">Done</ToggleButton>
                    <ToggleButton value={"unfinished"} size = "sm">Unfinished</ToggleButton>
                    </ToggleButtonGroup>
                </Col></Row>
                </Card.Footer>
            </Card>
    )
};

export default TodoList