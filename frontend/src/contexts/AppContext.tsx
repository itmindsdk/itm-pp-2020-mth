﻿import {createContext, Dispatch, SetStateAction, useReducer, useState} from "react";
import {TodoListEntityDto} from "../../NSwagTS/backend-api";
import ListReducer, {AllListActions} from "./ListReducer";


type AppContextType = {
    todoLists: TodoListEntityDto[],
    todoListDispatcher: Dispatch<AllListActions<TodoListEntityDto>>,
    showCreateTodoListModal: boolean,
    setShowStatus: Dispatch<SetStateAction<boolean>>,

}
export const appContext = createContext<AppContextType>(null);

export default function AppContextProvider({children}) {
    const [showCreateTodoListModal, setShowStatus] = useState(false);
    const [todoLists, todoListDispatcher] = useReducer(ListReducer<TodoListEntityDto>("id"), []);
    
    return (
        <appContext.Provider value={{todoLists, todoListDispatcher, showCreateTodoListModal, setShowStatus}}>
            {children}
        </appContext.Provider>
    );
};



 