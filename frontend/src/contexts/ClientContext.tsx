﻿import {createContext} from "react";
import {AuthClient, ExampleEntityClient, TodoListClient, TodoListItemClient} from "../../NSwagTS/backend-api";
import fetch from "isomorphic-unfetch";

type ClientContextType = {
    todoListClient : TodoListClient,
    todoListItemClient : TodoListItemClient
}
export const clientContext = createContext <ClientContextType>(null);

export default function ClientContextProvider({children}){
    

    return (
        <clientContext.Provider value={{
            todoListClient: new TodoListClient(new AuthClient("asd"), "https://localhost:5001", { fetch }),
           todoListItemClient : new TodoListItemClient(new AuthClient("asd"), "https://localhost:5001", {fetch})
        }}>
            {children}
        </clientContext.Provider>
    );
};



 