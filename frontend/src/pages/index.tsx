import {FunctionComponent, useContext, useEffect} from "react";
import {clientContext} from "../contexts/ClientContext"
import {appContext} from "../contexts/AppContext";
import TodoList from "../components/TodoList/TodoListComponent";
import NavBarComponent from "../components/NavBarComponent";

import CreateTodoListModalComponent from "../components/CreateTodoListModalComponent";
import {ListReducerActionType} from "../contexts/ListReducer";
import {TodoListItemClient, TodoListClient} from "../../NSwagTS/backend-api";

const Index: FunctionComponent = () => {
    const {todoListClient,todoListItemClient} = useContext(clientContext);
    const {todoLists, todoListDispatcher} = useContext(appContext);

    useEffect(() => {
        todoListItemClient.get().then(value => {
            todoListDispatcher({
                data: value.todoLists,
                type: ListReducerActionType.Reset

            })
        });
    }, [todoListClient, todoListDispatcher]);


    return (

        <div className="container">
            <NavBarComponent/>
            <CreateTodoListModalComponent/>
            {todoLists.map((todoList) => <TodoList todoList={todoList} key={todoList.id}/>)}
            {
                /*
    <div className="spacing">
        <Form>
            
            <h2>Delete list</h2>
  
  
            <Form.Group controlId="listId">
                <Form.Label>List id</Form.Label>
                <Form.Control type="text" placeholder="Enter list id" onChange={(e)=>{
                    setDeleteTodoListId(e.target.value);
  
                }}/>
            </Form.Group>
              
            <Button variant="primary" onClick = {()=>{
                exampleClient.delete(deleteTodoListId).then(value => {
                    exampleClient.get().then(value => setTodoLists(value.todoLists));
                    setDeleteTodoListId(null)
                });
            }}>
                Delete list
            </Button>
            
                 
                    
                </Form>
            </div>
            */
            }
        </div>


    );
};
export default Index;
