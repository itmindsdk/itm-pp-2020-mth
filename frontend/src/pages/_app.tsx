import 'bootstrap/dist/css/bootstrap.min.css';
import React from "react";
import ClientContextProvider from "../contexts/ClientContext";
import AppContextProvider from "../contexts/AppContext";


function MyApp({Component, pageProps}) {
    return (
            <ClientContextProvider>
                <AppContextProvider>
                <Component {...pageProps} />
                </AppContextProvider>
            </ClientContextProvider>
        
    )
}

export default MyApp