module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./NSwagTS/backend-api.ts":
/*!********************************!*\
  !*** ./NSwagTS/backend-api.ts ***!
  \********************************/
/*! exports provided: AuthClient, ClientBase, ExampleEntityClient, ExampleEntityListClient, TodoListClient, TodoListItemClient, CreateExampleEntityCommand, ExampleEnum, UpdateExampleEntityCommand, ExampleEntitiesViewModel, ExampleEnumDto, ExampleEntityDto, ExampleEntityListDto, CreateExampleEntityListCommand, CreateTodoListEntityCommand, UpdateTodoListEntityCommand, TodoListEntitiesViewModel, TodoListEntityDto, TodoListEntityViewModel, CreateTodoListItemEntityCommand, UpdateTodoListItemEntityCommand, TodoListsWithItemsEntitiesViewModel, TodoListWithItemsEntityDto, TodoListItemEntityDto, TodoListWithItemsEntityViewModel, TodoListItemEntityViewModel, SwaggerException */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthClient", function() { return AuthClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientBase", function() { return ClientBase; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntityClient", function() { return ExampleEntityClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntityListClient", function() { return ExampleEntityListClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListClient", function() { return TodoListClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListItemClient", function() { return TodoListItemClient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateExampleEntityCommand", function() { return CreateExampleEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEnum", function() { return ExampleEnum; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateExampleEntityCommand", function() { return UpdateExampleEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntitiesViewModel", function() { return ExampleEntitiesViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEnumDto", function() { return ExampleEnumDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntityDto", function() { return ExampleEntityDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExampleEntityListDto", function() { return ExampleEntityListDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateExampleEntityListCommand", function() { return CreateExampleEntityListCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateTodoListEntityCommand", function() { return CreateTodoListEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateTodoListEntityCommand", function() { return UpdateTodoListEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListEntitiesViewModel", function() { return TodoListEntitiesViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListEntityDto", function() { return TodoListEntityDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListEntityViewModel", function() { return TodoListEntityViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreateTodoListItemEntityCommand", function() { return CreateTodoListItemEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateTodoListItemEntityCommand", function() { return UpdateTodoListItemEntityCommand; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListsWithItemsEntitiesViewModel", function() { return TodoListsWithItemsEntitiesViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListWithItemsEntityDto", function() { return TodoListWithItemsEntityDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListItemEntityDto", function() { return TodoListItemEntityDto; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListWithItemsEntityViewModel", function() { return TodoListWithItemsEntityViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TodoListItemEntityViewModel", function() { return TodoListItemEntityViewModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SwaggerException", function() { return SwaggerException; });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/* tslint:disable */

/* eslint-disable */
//----------------------
// <auto-generated>
//     Generated using the NSwag toolchain v13.1.6.0 (NJsonSchema v10.0.28.0 (Newtonsoft.Json v12.0.0.0)) (http://NSwag.org)
// </auto-generated>
//----------------------
// ReSharper disable InconsistentNaming
class AuthClient {
  constructor(accessToken) {
    this.accessToken = accessToken;
  }

  transformHttpRequestOptions(options) {
    if (options.headers && this.accessToken) {
      options.headers['Authorization'] = 'Bearer ' + this.accessToken;
      return Promise.resolve(options);
    }
  }

}
class ClientBase {
  constructor(authClient) {
    this.authClient = authClient;
  }

  transformOptions(options) {
    return this.authClient ? this.authClient.transformHttpRequestOptions(options) : Promise.resolve(options);
  }

}
class ExampleEntityClient extends ClientBase {
  constructor(configuration, baseUrl, http) {
    super(configuration);

    _defineProperty(this, "http", void 0);

    _defineProperty(this, "baseUrl", void 0);

    _defineProperty(this, "jsonParseReviver", undefined);

    this.http = http ? http : window;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  create(command) {
    let url_ = this.baseUrl + "/api/ExampleEntity";
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processCreate(_response);
    });
  }

  processCreate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 !== undefined ? resultData200 : null;
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  get() {
    let url_ = this.baseUrl + "/api/ExampleEntity";
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "GET",
      headers: {
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processGet(_response);
    });
  }

  processGet(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = ExampleEntitiesViewModel.fromJS(resultData200);
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  update(id, command) {
    let url_ = this.baseUrl + "/api/ExampleEntity/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processUpdate(_response);
    });
  }

  processUpdate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  delete(id) {
    let url_ = this.baseUrl + "/api/ExampleEntity/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "DELETE",
      headers: {
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processDelete(_response);
    });
  }

  processDelete(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

}
class ExampleEntityListClient extends ClientBase {
  constructor(configuration, baseUrl, http) {
    super(configuration);

    _defineProperty(this, "http", void 0);

    _defineProperty(this, "baseUrl", void 0);

    _defineProperty(this, "jsonParseReviver", undefined);

    this.http = http ? http : window;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  create(command) {
    let url_ = this.baseUrl + "/api/ExampleEntityList";
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processCreate(_response);
    });
  }

  processCreate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 !== undefined ? resultData200 : null;
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

}
class TodoListClient extends ClientBase {
  constructor(configuration, baseUrl, http) {
    super(configuration);

    _defineProperty(this, "http", void 0);

    _defineProperty(this, "baseUrl", void 0);

    _defineProperty(this, "jsonParseReviver", undefined);

    this.http = http ? http : window;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  create(command) {
    let url_ = this.baseUrl + "/api/TodoList";
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processCreate(_response);
    });
  }

  processCreate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 !== undefined ? resultData200 : null;
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  get() {
    let url_ = this.baseUrl + "/api/TodoList";
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "GET",
      headers: {
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processGet(_response);
    });
  }

  processGet(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = TodoListEntitiesViewModel.fromJS(resultData200);
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  update(id, command) {
    let url_ = this.baseUrl + "/api/TodoList/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processUpdate(_response);
    });
  }

  processUpdate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  delete(id) {
    let url_ = this.baseUrl + "/api/TodoList/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "DELETE",
      headers: {
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processDelete(_response);
    });
  }

  processDelete(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  getId(id) {
    let url_ = this.baseUrl + "/api/TodoList/{id}";
    if (id === undefined || id === null) throw new Error("The parameter 'id' must be defined.");
    url_ = url_.replace("{id}", encodeURIComponent("" + id));
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "GET",
      headers: {
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processGetId(_response);
    });
  }

  processGetId(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = TodoListEntityViewModel.fromJS(resultData200);
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

}
class TodoListItemClient extends ClientBase {
  constructor(configuration, baseUrl, http) {
    super(configuration);

    _defineProperty(this, "http", void 0);

    _defineProperty(this, "baseUrl", void 0);

    _defineProperty(this, "jsonParseReviver", undefined);

    this.http = http ? http : window;
    this.baseUrl = baseUrl ? baseUrl : "";
  }

  create(command, listId) {
    let url_ = this.baseUrl + "/api/TodoList/{listId}/item";
    if (listId === undefined || listId === null) throw new Error("The parameter 'listId' must be defined.");
    url_ = url_.replace("{listId}", encodeURIComponent("" + listId));
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processCreate(_response);
    });
  }

  processCreate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = resultData200 !== undefined ? resultData200 : null;
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  getSingleList(listId) {
    let url_ = this.baseUrl + "/api/TodoList/{listId}/item";
    if (listId === undefined || listId === null) throw new Error("The parameter 'listId' must be defined.");
    url_ = url_.replace("{listId}", encodeURIComponent("" + listId));
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "GET",
      headers: {
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processGetSingleList(_response);
    });
  }

  processGetSingleList(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = TodoListWithItemsEntityViewModel.fromJS(resultData200);
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  update(listId, itemId, command) {
    let url_ = this.baseUrl + "/api/TodoList/{listId}/item/{itemId}";
    if (listId === undefined || listId === null) throw new Error("The parameter 'listId' must be defined.");
    url_ = url_.replace("{listId}", encodeURIComponent("" + listId));
    if (itemId === undefined || itemId === null) throw new Error("The parameter 'itemId' must be defined.");
    url_ = url_.replace("{itemId}", encodeURIComponent("" + itemId));
    url_ = url_.replace(/[?&]$/, "");
    const content_ = JSON.stringify(command);
    let options_ = {
      body: content_,
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processUpdate(_response);
    });
  }

  processUpdate(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  delete(listId, itemId) {
    let url_ = this.baseUrl + "/api/TodoList/{listId}/item/{itemId}";
    if (listId === undefined || listId === null) throw new Error("The parameter 'listId' must be defined.");
    url_ = url_.replace("{listId}", encodeURIComponent("" + listId));
    if (itemId === undefined || itemId === null) throw new Error("The parameter 'itemId' must be defined.");
    url_ = url_.replace("{itemId}", encodeURIComponent("" + itemId));
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "DELETE",
      headers: {
        "Accept": "application/octet-stream"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processDelete(_response);
    });
  }

  processDelete(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200 || status === 206) {
      const contentDisposition = response.headers ? response.headers.get("content-disposition") : undefined;
      const fileNameMatch = contentDisposition ? /filename="?([^"]*?)"?(;|$)/g.exec(contentDisposition) : undefined;
      const fileName = fileNameMatch && fileNameMatch.length > 1 ? fileNameMatch[1] : undefined;
      return response.blob().then(blob => {
        return {
          fileName: fileName,
          data: blob,
          status: status,
          headers: _headers
        };
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  getSingleItem(listId, itemId) {
    let url_ = this.baseUrl + "/api/TodoList/{listId}/item/{itemId}";
    if (listId === undefined || listId === null) throw new Error("The parameter 'listId' must be defined.");
    url_ = url_.replace("{listId}", encodeURIComponent("" + listId));
    if (itemId === undefined || itemId === null) throw new Error("The parameter 'itemId' must be defined.");
    url_ = url_.replace("{itemId}", encodeURIComponent("" + itemId));
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "GET",
      headers: {
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processGetSingleItem(_response);
    });
  }

  processGetSingleItem(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = TodoListItemEntityViewModel.fromJS(resultData200);
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

  get() {
    let url_ = this.baseUrl + "/api/TodoList/item";
    url_ = url_.replace(/[?&]$/, "");
    let options_ = {
      method: "GET",
      headers: {
        "Accept": "application/json"
      }
    };
    return this.transformOptions(options_).then(transformedOptions_ => {
      return this.http.fetch(url_, transformedOptions_);
    }).then(_response => {
      return this.processGet(_response);
    });
  }

  processGet(response) {
    const status = response.status;
    let _headers = {};

    if (response.headers && response.headers.forEach) {
      response.headers.forEach((v, k) => _headers[k] = v);
    }

    ;

    if (status === 200) {
      return response.text().then(_responseText => {
        let result200 = null;
        let resultData200 = _responseText === "" ? null : JSON.parse(_responseText, this.jsonParseReviver);
        result200 = TodoListsWithItemsEntitiesViewModel.fromJS(resultData200);
        return result200;
      });
    } else if (status !== 200 && status !== 204) {
      return response.text().then(_responseText => {
        return throwException("An unexpected server error occurred.", status, _responseText, _headers);
      });
    }

    return Promise.resolve(null);
  }

}
class CreateExampleEntityCommand {
  constructor(data) {
    _defineProperty(this, "name", void 0);

    _defineProperty(this, "exampleEnum", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.name = _data["name"];
      this.exampleEnum = _data["exampleEnum"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new CreateExampleEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["name"] = this.name;
    data["exampleEnum"] = this.exampleEnum;
    return data;
  }

}
let ExampleEnum;

(function (ExampleEnum) {
  ExampleEnum[ExampleEnum["A"] = 0] = "A";
  ExampleEnum[ExampleEnum["B"] = 1] = "B";
  ExampleEnum[ExampleEnum["C"] = 2] = "C";
  ExampleEnum[ExampleEnum["D"] = 3] = "D";
})(ExampleEnum || (ExampleEnum = {}));

class UpdateExampleEntityCommand {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    _defineProperty(this, "exampleEnum", void 0);

    _defineProperty(this, "exampleEntityListId", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
      this.exampleEnum = _data["exampleEnum"];
      this.exampleEntityListId = _data["exampleEntityListId"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new UpdateExampleEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    data["exampleEnum"] = this.exampleEnum;
    data["exampleEntityListId"] = this.exampleEntityListId;
    return data;
  }

}
class ExampleEntitiesViewModel {
  constructor(data) {
    _defineProperty(this, "exampleEnum", void 0);

    _defineProperty(this, "exampleEntities", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      if (Array.isArray(_data["exampleEnum"])) {
        this.exampleEnum = [];

        for (let item of _data["exampleEnum"]) this.exampleEnum.push(ExampleEnumDto.fromJS(item));
      }

      if (Array.isArray(_data["exampleEntities"])) {
        this.exampleEntities = [];

        for (let item of _data["exampleEntities"]) this.exampleEntities.push(ExampleEntityDto.fromJS(item));
      }
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new ExampleEntitiesViewModel();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};

    if (Array.isArray(this.exampleEnum)) {
      data["exampleEnum"] = [];

      for (let item of this.exampleEnum) data["exampleEnum"].push(item.toJSON());
    }

    if (Array.isArray(this.exampleEntities)) {
      data["exampleEntities"] = [];

      for (let item of this.exampleEntities) data["exampleEntities"].push(item.toJSON());
    }

    return data;
  }

}
class ExampleEnumDto {
  constructor(data) {
    _defineProperty(this, "value", void 0);

    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.value = _data["value"];
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new ExampleEnumDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["value"] = this.value;
    data["name"] = this.name;
    return data;
  }

}
class ExampleEntityDto {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    _defineProperty(this, "exampleEntityList", void 0);

    _defineProperty(this, "exampleEnum", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
      this.exampleEntityList = _data["exampleEntityList"] ? ExampleEntityListDto.fromJS(_data["exampleEntityList"]) : undefined;
      this.exampleEnum = _data["exampleEnum"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new ExampleEntityDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    data["exampleEntityList"] = this.exampleEntityList ? this.exampleEntityList.toJSON() : undefined;
    data["exampleEnum"] = this.exampleEnum;
    return data;
  }

}
class ExampleEntityListDto {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new ExampleEntityListDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    return data;
  }

}
class CreateExampleEntityListCommand {
  constructor(data) {
    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new CreateExampleEntityListCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["name"] = this.name;
    return data;
  }

}
class CreateTodoListEntityCommand {
  constructor(data) {
    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new CreateTodoListEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["name"] = this.name;
    return data;
  }

}
class UpdateTodoListEntityCommand {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new UpdateTodoListEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    return data;
  }

}
class TodoListEntitiesViewModel {
  constructor(data) {
    _defineProperty(this, "todoLists", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      if (Array.isArray(_data["todoLists"])) {
        this.todoLists = [];

        for (let item of _data["todoLists"]) this.todoLists.push(TodoListEntityDto.fromJS(item));
      }
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListEntitiesViewModel();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};

    if (Array.isArray(this.todoLists)) {
      data["todoLists"] = [];

      for (let item of this.todoLists) data["todoLists"].push(item.toJSON());
    }

    return data;
  }

}
class TodoListEntityDto {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListEntityDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;
    return data;
  }

}
class TodoListEntityViewModel {
  constructor(data) {
    _defineProperty(this, "todoList", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.todoList = _data["todoList"] ? TodoListEntityDto.fromJS(_data["todoList"]) : undefined;
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListEntityViewModel();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["todoList"] = this.todoList ? this.todoList.toJSON() : undefined;
    return data;
  }

}
class CreateTodoListItemEntityCommand {
  constructor(data) {
    _defineProperty(this, "listId", void 0);

    _defineProperty(this, "itemName", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.listId = _data["listId"];
      this.itemName = _data["itemName"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new CreateTodoListItemEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["listId"] = this.listId;
    data["itemName"] = this.itemName;
    return data;
  }

}
class UpdateTodoListItemEntityCommand {
  constructor(data) {
    _defineProperty(this, "listId", void 0);

    _defineProperty(this, "itemId", void 0);

    _defineProperty(this, "itemName", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.listId = _data["listId"];
      this.itemId = _data["itemId"];
      this.itemName = _data["itemName"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new UpdateTodoListItemEntityCommand();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["listId"] = this.listId;
    data["itemId"] = this.itemId;
    data["itemName"] = this.itemName;
    return data;
  }

}
class TodoListsWithItemsEntitiesViewModel {
  constructor(data) {
    _defineProperty(this, "todoLists", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      if (Array.isArray(_data["todoLists"])) {
        this.todoLists = [];

        for (let item of _data["todoLists"]) this.todoLists.push(TodoListWithItemsEntityDto.fromJS(item));
      }
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListsWithItemsEntitiesViewModel();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};

    if (Array.isArray(this.todoLists)) {
      data["todoLists"] = [];

      for (let item of this.todoLists) data["todoLists"].push(item.toJSON());
    }

    return data;
  }

}
class TodoListWithItemsEntityDto {
  constructor(data) {
    _defineProperty(this, "id", void 0);

    _defineProperty(this, "name", void 0);

    _defineProperty(this, "listItems", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.id = _data["id"];
      this.name = _data["name"];

      if (Array.isArray(_data["listItems"])) {
        this.listItems = [];

        for (let item of _data["listItems"]) this.listItems.push(TodoListItemEntityDto.fromJS(item));
      }
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListWithItemsEntityDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["id"] = this.id;
    data["name"] = this.name;

    if (Array.isArray(this.listItems)) {
      data["listItems"] = [];

      for (let item of this.listItems) data["listItems"].push(item.toJSON());
    }

    return data;
  }

}
class TodoListItemEntityDto {
  constructor(data) {
    _defineProperty(this, "itemId", void 0);

    _defineProperty(this, "itemName", void 0);

    _defineProperty(this, "completed", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.itemId = _data["itemId"];
      this.itemName = _data["itemName"];
      this.completed = _data["completed"];
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListItemEntityDto();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["itemId"] = this.itemId;
    data["itemName"] = this.itemName;
    data["completed"] = this.completed;
    return data;
  }

}
class TodoListWithItemsEntityViewModel {
  constructor(data) {
    _defineProperty(this, "todoList", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.todoList = _data["todoList"] ? TodoListWithItemsEntityDto.fromJS(_data["todoList"]) : undefined;
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListWithItemsEntityViewModel();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["todoList"] = this.todoList ? this.todoList.toJSON() : undefined;
    return data;
  }

}
class TodoListItemEntityViewModel {
  constructor(data) {
    _defineProperty(this, "listItem", void 0);

    if (data) {
      for (var property in data) {
        if (data.hasOwnProperty(property)) this[property] = data[property];
      }
    }
  }

  init(_data) {
    if (_data) {
      this.listItem = _data["listItem"] ? TodoListItemEntityDto.fromJS(_data["listItem"]) : undefined;
    }
  }

  static fromJS(data) {
    data = typeof data === 'object' ? data : {};
    let result = new TodoListItemEntityViewModel();
    result.init(data);
    return result;
  }

  toJSON(data) {
    data = typeof data === 'object' ? data : {};
    data["listItem"] = this.listItem ? this.listItem.toJSON() : undefined;
    return data;
  }

}
class SwaggerException extends Error {
  constructor(message, status, response, headers, result) {
    super();

    _defineProperty(this, "message", void 0);

    _defineProperty(this, "status", void 0);

    _defineProperty(this, "response", void 0);

    _defineProperty(this, "headers", void 0);

    _defineProperty(this, "result", void 0);

    _defineProperty(this, "isSwaggerException", true);

    this.message = message;
    this.status = status;
    this.response = response;
    this.headers = headers;
    this.result = result;
  }

  static isSwaggerException(obj) {
    return obj.isSwaggerException === true;
  }

}

function throwException(message, status, response, headers, result) {
  if (result !== null && result !== undefined) throw result;else throw new SwaggerException(message, status, response, headers, null);
}

/***/ }),

/***/ "./src/components/CreateTodoListModalComponent.tsx":
/*!*********************************************************!*\
  !*** ./src/components/CreateTodoListModalComponent.tsx ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _contexts_AppContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../contexts/AppContext */ "./src/contexts/AppContext.tsx");
/* harmony import */ var _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../NSwagTS/backend-api */ "./NSwagTS/backend-api.ts");
/* harmony import */ var _contexts_ListReducer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../contexts/ListReducer */ "./src/contexts/ListReducer.ts");
/* harmony import */ var _contexts_ClientContext__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../contexts/ClientContext */ "./src/contexts/ClientContext.tsx");
var _jsxFileName = "C:\\Users\\mth\\RiderProjects\\itm-pp-2020-mth\\frontend\\src\\components\\CreateTodoListModalComponent.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;







const CreateTodoListModalComponent = () => {
  const {
    0: createTodoListCommand,
    1: setCreateTodoListCommand
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_3__["CreateTodoListEntityCommand"]());
  const {
    todoListClient
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_ClientContext__WEBPACK_IMPORTED_MODULE_5__["clientContext"]);
  let {
    showCreateTodoListModal,
    setShowStatus,
    todoListDispatcher
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_AppContext__WEBPACK_IMPORTED_MODULE_2__["appContext"]);
  const addTodoList = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(createTodoListCommand => {
    todoListClient.create(createTodoListCommand).then(value => {
      todoListDispatcher({
        type: _contexts_ListReducer__WEBPACK_IMPORTED_MODULE_4__["ListReducerActionType"].Add,
        data: new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_3__["TodoListEntityDto"]({
          id: value,
          name: createTodoListCommand.name
        })
      });
      setCreateTodoListCommand(new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_3__["CreateTodoListEntityCommand"]({}));
      setShowStatus(false);
    });
  }, [todoListDispatcher]);

  const handleChange = e => {
    setCreateTodoListCommand(new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_3__["CreateTodoListEntityCommand"]({
      name: e.target.value
    }));
  };

  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"], {
    show: showCreateTodoListModal,
    onHide: () => {
      setShowStatus(false);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"].Header, {
    closeButton: true,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"].Title, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: undefined
  }, "Create new todo list")), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"].Body, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Group, {
    controlId: "listName",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Label, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: undefined
  }, "List Name"), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Form"].Control, {
    type: "text",
    placeholder: "Enter list name",
    onChange: handleChange,
    value: createTodoListCommand.name,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: undefined
  })))), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Modal"].Footer, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    variant: "secondary",
    onClick: () => {
      setShowStatus(false);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: undefined
  }, "Close"), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    variant: "primary",
    onClick: () => addTodoList(createTodoListCommand),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: undefined
  }, "Create todo list"))));
};

/* harmony default export */ __webpack_exports__["default"] = (CreateTodoListModalComponent);

/***/ }),

/***/ "./src/components/NavBarComponent.tsx":
/*!********************************************!*\
  !*** ./src/components/NavBarComponent.tsx ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _contexts_AppContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../contexts/AppContext */ "./src/contexts/AppContext.tsx");
var _jsxFileName = "C:\\Users\\mth\\RiderProjects\\itm-pp-2020-mth\\frontend\\src\\components\\NavBarComponent.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;




const NavBarComponent = () => {
  let {
    setShowStatus
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_AppContext__WEBPACK_IMPORTED_MODULE_2__["appContext"]);
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"], {
    bg: "light",
    expand: "lg",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"].Brand, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: undefined
  }, "Todo app"), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"].Toggle, {
    "aria-controls": "basic-navbar-nav",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: undefined
  }), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"].Collapse, {
    id: "basic-navbar-nav",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Nav"], {
    className: "mr-auto",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Button"], {
    onClick: () => {
      setShowStatus(true);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: undefined
  }, "Create new todo list")))));
};

/* harmony default export */ __webpack_exports__["default"] = (NavBarComponent);

/***/ }),

/***/ "./src/components/TodoListComponent.tsx":
/*!**********************************************!*\
  !*** ./src/components/TodoListComponent.tsx ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../NSwagTS/backend-api */ "./NSwagTS/backend-api.ts");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _contexts_ListReducer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../contexts/ListReducer */ "./src/contexts/ListReducer.ts");
/* harmony import */ var _contexts_ClientContext__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../contexts/ClientContext */ "./src/contexts/ClientContext.tsx");
var _jsxFileName = "C:\\Users\\mth\\RiderProjects\\itm-pp-2020-mth\\frontend\\src\\components\\TodoListComponent.tsx";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






const TodoList = ({
  todoList
}) => {
  const {
    0: createTodoListItemCommand,
    1: setCreateTodoListItemCommand
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["CreateTodoListItemEntityCommand"]());
  const {
    todoListItemClient
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_ClientContext__WEBPACK_IMPORTED_MODULE_4__["clientContext"]);
  const {
    0: todoItemList,
    1: todoItemListDispatcher
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useReducer"])(Object(_contexts_ListReducer__WEBPACK_IMPORTED_MODULE_3__["default"])("itemId"), todoList.listItems);
  const addTodoItem = Object(react__WEBPACK_IMPORTED_MODULE_0__["useCallback"])(createTodoListItemEntityCommand => {
    todoListItemClient.create(createTodoListItemEntityCommand, todoList.id.toString()).then(value => {
      todoItemListDispatcher({
        type: _contexts_ListReducer__WEBPACK_IMPORTED_MODULE_3__["ListReducerActionType"].Add,
        data: new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["TodoListItemEntityDto"]({
          completed: false,
          itemId: value,
          itemName: createTodoListItemEntityCommand.itemName
        })
      });
      setCreateTodoListItemCommand(new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["CreateTodoListItemEntityCommand"]({
        itemName: ""
      }));
    });
  }, [todoItemListDispatcher]);
  return __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Card"].Header, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: undefined
  }, todoList.id, " - ", todoList.name), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["InputGroup"], {
    className: "mb-3",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 36
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["FormControl"], {
    placeholder: "Todo item name..",
    onChange: e => {
      setCreateTodoListItemCommand(new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["CreateTodoListItemEntityCommand"]({
        itemName: e.target.value,
        listId: todoList.id
      }));
      console.log(createTodoListItemCommand);
    },
    value: createTodoListItemCommand.itemName,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 37
    },
    __self: undefined
  }), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["InputGroup"].Append, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 46
    },
    __self: undefined
  }, __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["Button"], {
    variant: "primary",
    onClick: () => {
      addTodoItem(createTodoListItemCommand);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 47
    },
    __self: undefined
  }, "Add new todo item"))), __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ListGroup"], {
    variant: "flush",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: undefined
  }, todoItemList.map(item => __jsx(react_bootstrap__WEBPACK_IMPORTED_MODULE_2__["ListGroup"].Item, {
    key: item.itemId,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: undefined
  }, " ", item.itemId, "-", item.itemName, " - Completed: ", item.completed.toString()))));
};

/* harmony default export */ __webpack_exports__["default"] = (TodoList);

/***/ }),

/***/ "./src/contexts/AppContext.tsx":
/*!*************************************!*\
  !*** ./src/contexts/AppContext.tsx ***!
  \*************************************/
/*! exports provided: appContext, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "appContext", function() { return appContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return AppContextProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ListReducer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ListReducer */ "./src/contexts/ListReducer.ts");
var _jsxFileName = "C:\\Users\\mth\\RiderProjects\\itm-pp-2020-mth\\frontend\\src\\contexts\\AppContext.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


const appContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])(null);
function AppContextProvider({
  children
}) {
  const {
    0: showCreateTodoListModal,
    1: setShowStatus
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(false);
  const {
    0: todoLists,
    1: todoListDispatcher
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useReducer"])(Object(_ListReducer__WEBPACK_IMPORTED_MODULE_1__["default"])("id"), []);
  return __jsx(appContext.Provider, {
    value: {
      todoLists,
      todoListDispatcher,
      showCreateTodoListModal,
      setShowStatus
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, children);
}
;

/***/ }),

/***/ "./src/contexts/ClientContext.tsx":
/*!****************************************!*\
  !*** ./src/contexts/ClientContext.tsx ***!
  \****************************************/
/*! exports provided: clientContext, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "clientContext", function() { return clientContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return ClientContextProvider; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../NSwagTS/backend-api */ "./NSwagTS/backend-api.ts");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! isomorphic-unfetch */ "isomorphic-unfetch");
/* harmony import */ var isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "C:\\Users\\mth\\RiderProjects\\itm-pp-2020-mth\\frontend\\src\\contexts\\ClientContext.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const clientContext = Object(react__WEBPACK_IMPORTED_MODULE_0__["createContext"])(null);
function ClientContextProvider({
  children
}) {
  return __jsx(clientContext.Provider, {
    value: {
      todoListClient: new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["TodoListClient"](new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["AuthClient"]("asd"), "https://localhost:5001", {
        fetch: (isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default())
      }),
      todoListItemClient: new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["TodoListItemClient"](new _NSwagTS_backend_api__WEBPACK_IMPORTED_MODULE_1__["AuthClient"]("asd"), "https://localhost:5001", {
        fetch: (isomorphic_unfetch__WEBPACK_IMPORTED_MODULE_2___default())
      })
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }, children);
}
;

/***/ }),

/***/ "./src/contexts/ListReducer.ts":
/*!*************************************!*\
  !*** ./src/contexts/ListReducer.ts ***!
  \*************************************/
/*! exports provided: ListReducerActionType, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListReducerActionType", function() { return ListReducerActionType; });
/**
 * Different mutating types to be performed in the reducers dispatch.
 */
let ListReducerActionType; //The conditional type, specifying the type of data depending on the action type

(function (ListReducerActionType) {
  ListReducerActionType[ListReducerActionType["Remove"] = 0] = "Remove";
  ListReducerActionType[ListReducerActionType["Update"] = 1] = "Update";
  ListReducerActionType[ListReducerActionType["Add"] = 2] = "Add";
  ListReducerActionType[ListReducerActionType["AddOrUpdate"] = 3] = "AddOrUpdate";
  ListReducerActionType[ListReducerActionType["Reset"] = 4] = "Reset";
})(ListReducerActionType || (ListReducerActionType = {}));

/**
 *
 * @typeparam `T` type of the reducer state
 * @param {keyof T} key value of `U`
 * @return {Reducer} React reducer for a stateful list of `T`
 *
 * Can be initiated like this
 * `listReducer<Entity>("id")`
 * Where `Entity` is the type of the list
 * and `"id"` is a property key on the type
 * that is to be used to find index in the list
 */
/* harmony default export */ __webpack_exports__["default"] = (key => (state, action) => {
  const replace = t => {
    const index = state.findIndex(i => i[key] === t[key]);
    state[index] = t;
  };

  switch (action.type) {
    case ListReducerActionType.AddOrUpdate:
      if (action.data.push) {
        console.log("what?");
      } else {
        const index = state.findIndex(i => i[key] === action.data[key]);

        if (index !== -1) {
          replace(action.data);
          return [...state];
        } else {
          return [...state, action.data];
        }
      }

    case ListReducerActionType.Add:
      if (action.data.push) {
        return [...state, ...action.data];
      } else {
        return [...state, action.data];
      }

    case ListReducerActionType.Update:
      {
        if (action.data.push) {
          action.data.forEach(replace);
        } else {
          replace(action.data);
        }

        return [...state];
      }

    case ListReducerActionType.Remove:
      if (action.data.push) {
        return state.filter(t => action.data.indexOf(t[key]) === -1);
      } else {
        return state.filter(t => t[key] !== action.data);
      }

    case ListReducerActionType.Reset:
      return action.data;

    default:
      return state;
  }
});

/***/ }),

/***/ "./src/pages/index.tsx":
/*!*****************************!*\
  !*** ./src/pages/index.tsx ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _contexts_ClientContext__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../contexts/ClientContext */ "./src/contexts/ClientContext.tsx");
/* harmony import */ var _contexts_AppContext__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../contexts/AppContext */ "./src/contexts/AppContext.tsx");
/* harmony import */ var _components_TodoListComponent__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/TodoListComponent */ "./src/components/TodoListComponent.tsx");
/* harmony import */ var _components_NavBarComponent__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/NavBarComponent */ "./src/components/NavBarComponent.tsx");
/* harmony import */ var _components_CreateTodoListModalComponent__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/CreateTodoListModalComponent */ "./src/components/CreateTodoListModalComponent.tsx");
/* harmony import */ var _contexts_ListReducer__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../contexts/ListReducer */ "./src/contexts/ListReducer.ts");
var _jsxFileName = "C:\\Users\\mth\\RiderProjects\\itm-pp-2020-mth\\frontend\\src\\pages\\index.tsx";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;








const Index = () => {
  const {
    todoListClient,
    todoListItemClient
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_ClientContext__WEBPACK_IMPORTED_MODULE_1__["clientContext"]);
  const {
    todoLists,
    todoListDispatcher
  } = Object(react__WEBPACK_IMPORTED_MODULE_0__["useContext"])(_contexts_AppContext__WEBPACK_IMPORTED_MODULE_2__["appContext"]);
  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(() => {
    todoListItemClient.get().then(value => {
      todoListDispatcher({
        data: value.todoLists,
        type: _contexts_ListReducer__WEBPACK_IMPORTED_MODULE_6__["ListReducerActionType"].Reset
      });
    });
  }, [todoListClient, todoListDispatcher]);
  return __jsx("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: undefined
  }, __jsx(_components_NavBarComponent__WEBPACK_IMPORTED_MODULE_4__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: undefined
  }), __jsx(_components_CreateTodoListModalComponent__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: undefined
  }), todoLists.map(todoList => __jsx(_components_TodoListComponent__WEBPACK_IMPORTED_MODULE_3__["default"], {
    todoList: todoList,
    key: todoList.id,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: undefined
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (Index);

/***/ }),

/***/ 4:
/*!***********************************!*\
  !*** multi ./src/pages/index.tsx ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\mth\RiderProjects\itm-pp-2020-mth\frontend\src\pages\index.tsx */"./src/pages/index.tsx");


/***/ }),

/***/ "isomorphic-unfetch":
/*!*************************************!*\
  !*** external "isomorphic-unfetch" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("isomorphic-unfetch");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map