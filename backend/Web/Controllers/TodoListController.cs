﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ExampleEntities.Commands.CreateExampleEntity;
using Application.ExampleEntities.Commands.DeleteExampleEntity;
using Application.ExampleEntities.Commands.UpdateExampleEntity;
using Application.ExampleEntities.Queries.GetExampleEntities;
using Application.TodoListEntities.Commands.CreateTodoListEntity;
using Application.TodoListEntities.Commands.DeleteTodoListEntity;
using Application.TodoListEntities.Commands.UpdateTodoListEntity;
using Application.TodoListEntities.Queries.GetTodoListEntities;
using Application.TodoListEntities.Queries.GetTodoListEntity;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{

    public class TodoListcontroller : ApiControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateTodoListEntityCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdateTodoListEntityCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteTodoListEntityCommand()
            {
                Id = id
            });
            return NoContent();
        }

        [HttpGet]
        public async Task<ActionResult<TodoListEntitiesViewModel>> Get()
        {
            return await Mediator.Send(new GetTodoListEntitiesQuery());
        }
        
        
        
        [HttpGet("{id}")]
        public async Task<ActionResult<TodoListEntityViewModel>> GetId(int id)
        {
            return await Mediator.Send(new GetTodoListEntityQuery(){Id = id});
        }

    }
}
