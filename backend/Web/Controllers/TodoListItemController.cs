﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.ExampleEntities.Commands.CreateExampleEntity;
using Application.ExampleEntities.Commands.DeleteExampleEntity;
using Application.ExampleEntities.Commands.UpdateExampleEntity;
using Application.ExampleEntities.Queries.GetExampleEntities;
using Application.TodoListEntities.Commands.CreateTodoListEntity;
using Application.TodoListEntities.Commands.DeleteTodoListEntity;
using Application.TodoListEntities.Commands.UpdateTodoListEntity;
using Application.TodoListEntities.Queries.GetTodoListEntities;
using Application.TodoListEntities.Queries.GetTodoListEntity;
using Application.TodoListItemEntities.Commands.CreateTodoListItemEntity;
using Application.TodoListItemEntities.Commands.DeleteTodoListItemEntity;
using Application.TodoListItemEntities.Commands.UpdateTodoListItemEntity;
using Application.TodoListItemEntities.Queries.GetTodoListItem;
using Application.TodoListItemEntities.Queries.GetTodoListsWithItemsEntities;
using Application.TodoListItemEntities.Queries.GetTodoListWithItemsEntity;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Web.Controllers
{
    [Route("api/TodoList/")]
    public class TodoListItemController : ApiControllerBase
    {
        
        [HttpPost("{listId}/item")]
        public async Task<ActionResult<int>> Create(CreateTodoListItemEntityCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{listId}/item/{itemId}")]
        public async Task<ActionResult> Update(int listId, int itemId, UpdateTodoListItemEntityCommand command)
        {
            if (itemId != command.ItemId)
            {
                return BadRequest();
            }
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{listId}/item/{itemId}")]
        public async Task<ActionResult> Delete(int listId, int itemId)
        {
            await Mediator.Send(new DeleteTodoListItemEntityCommand()
            {
                ListId =  listId,
                ItemId = itemId
            });
            return NoContent();
        }

        [HttpGet("item/")]
        public async Task<ActionResult<TodoListsWithItemsEntitiesViewModel>> Get()
        {
            return await Mediator.Send(new GetTodoListsWithItemsEntitiesQuery());
        }
        
        
        [HttpGet("{listId}/item/")]
        public async Task<ActionResult<TodoListWithItemsEntityViewModel>> GetSingleList(int listId)
        {
            return await Mediator.Send(new GetTodoListWithItemsEntityQuery(){ListId = listId});
        }
        
        [HttpGet("{listId}/item/{itemId}")]
        public async Task<ActionResult<TodoListItemEntityViewModel>> GetSingleItem(int listId, int itemId)
        {
            return await Mediator.Send(new GetTodoListItemEntityQuery(){ListId = listId, ItemId = itemId});
        }


    }
}
