﻿using System;
using System.Collections.Generic;
using System.Text;
using Application.Common.Interfaces;

namespace Web.IntegrationTests
{
    public class TestDateTimeService : IDateTimeOffset
    {
        public DateTimeOffset Now => DateTimeOffset.Now;
    }
}
