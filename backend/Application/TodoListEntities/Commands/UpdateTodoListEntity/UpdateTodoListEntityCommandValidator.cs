﻿using Application.ExampleEntities.Commands.UpdateExampleEntity;
using FluentValidation;

namespace Application.TodoListEntities.Commands.UpdateTodoListEntity
{
    public class UpdateTodoListEntityCommandValidator : AbstractValidator<UpdateTodoListEntityCommand>
    {

        public UpdateTodoListEntityCommandValidator()
        {
            RuleFor(e => e.Name)
                .MaximumLength(25)
                .NotEmpty();
        }
    }
}
