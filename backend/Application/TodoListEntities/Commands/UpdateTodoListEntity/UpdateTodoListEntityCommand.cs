﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListEntities.Commands.UpdateTodoListEntity
{
    public class UpdateTodoListEntityCommand : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }


        public class UpdateTodoListEntityCommandHandler : IRequestHandler<UpdateTodoListEntityCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateTodoListEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateTodoListEntityCommand request, CancellationToken cancellationToken)
            {
               var todoListEntity = await _context.TodoListEntities.FindAsync(request.Id);

               if (todoListEntity == null)
               {
                   throw  new NotFoundException(nameof(ExampleEntity), request.Id);
               }
               

               

               todoListEntity.ListName = request.Name;

               _context.TodoListEntities.Update(todoListEntity);
               await _context.SaveChangesAsync(cancellationToken);

               return Unit.Value;
            }
        }
    }
}
