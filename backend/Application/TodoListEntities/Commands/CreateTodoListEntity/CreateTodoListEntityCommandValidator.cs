﻿using Application.ExampleEntities.Commands.CreateExampleEntity;
using FluentValidation;

namespace Application.TodoListEntities.Commands.CreateTodoListEntity
{
    public class CreateTodoListEntityCommandValidator : AbstractValidator<CreateTodoListEntityCommand>
    {
        public CreateTodoListEntityCommandValidator()
        {
            RuleFor(e => e.Name)
                .MaximumLength(25)
                .NotEmpty();
        }
    }
}
