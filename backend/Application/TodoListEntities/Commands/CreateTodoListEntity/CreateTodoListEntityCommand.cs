﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Domain.Entities;
using Domain.Enums;
using MediatR;

namespace Application.TodoListEntities.Commands.CreateTodoListEntity
{
    public class CreateTodoListEntityCommand : IRequest<int>
    {
        public string Name { get; set; }

        public class CreateTodoListEntityCommandHandler : IRequestHandler<CreateTodoListEntityCommand, int>
        {
            private readonly IApplicationDbContext _context;

            public CreateTodoListEntityCommandHandler (IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(CreateTodoListEntityCommand request, CancellationToken cancellationToken)
            {
                var todoListEntity = new TodoListEntity()
                {
                    ListName = request.Name
                };

                _context.TodoListEntities.Add(todoListEntity);

                await _context.SaveChangesAsync(cancellationToken);

                return todoListEntity.Id;
            }
        }
    }
}
