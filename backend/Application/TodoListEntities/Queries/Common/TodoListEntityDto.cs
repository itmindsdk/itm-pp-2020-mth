﻿using System.Collections.Generic;
using Application.Common.Mappings;
using AutoMapper;
using Domain.Entities;

namespace Application.TodoListEntities.Queries.Common
{
    public class TodoListEntityDto : IMapFrom<TodoListEntity>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap<TodoListEntity, TodoListEntityDto>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.ListName));


        }
    }
}
