﻿using System.Collections.Generic;
using Application.TodoListEntities.Queries.Common;

namespace Application.TodoListEntities.Queries.GetTodoListEntities
{
    public class TodoListEntitiesViewModel
    {
        public IList<TodoListEntityDto> TodoLists { get; set; }

    }
}
