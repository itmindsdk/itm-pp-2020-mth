﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.ExampleEntities.Queries.GetExampleEntities;
using Application.TodoListEntities.Queries.Common;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListEntities.Queries.GetTodoListEntities
{
    public class GetTodoListEntitiesQuery : IRequest<TodoListEntitiesViewModel>
    {
        public class GetTodoListEntitiesQueryHandler : IRequestHandler<GetTodoListEntitiesQuery, TodoListEntitiesViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodoListEntitiesQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<TodoListEntitiesViewModel> Handle(GetTodoListEntitiesQuery request, CancellationToken cancellationToken)
            {
                TodoListEntitiesViewModel viewModel = new TodoListEntitiesViewModel();
                
                viewModel.TodoLists = await _context.TodoListEntities
                    .ProjectTo<TodoListEntityDto>(_mapper.ConfigurationProvider)
                        .ToListAsync(cancellationToken);

                return viewModel;
            }
        }
    }
}
