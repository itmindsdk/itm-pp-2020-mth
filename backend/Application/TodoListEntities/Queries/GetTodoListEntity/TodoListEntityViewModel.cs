﻿using System.Collections.Generic;
using Application.TodoListEntities.Queries.Common;

namespace Application.TodoListEntities.Queries.GetTodoListEntities
{
    public class TodoListEntityViewModel
    {
        public TodoListEntityDto TodoList { get; set; }

    }
}
