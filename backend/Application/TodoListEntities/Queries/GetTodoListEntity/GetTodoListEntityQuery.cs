﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.TodoListEntities.Queries.Common;
using Application.TodoListEntities.Queries.GetTodoListEntities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListEntities.Queries.GetTodoListEntity
{
    public class GetTodoListEntityQuery : IRequest<TodoListEntityViewModel>
    {
        public int Id { get; set; }

        public class GetTodoListEntityQueryHandler : IRequestHandler<GetTodoListEntityQuery, TodoListEntityViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodoListEntityQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<TodoListEntityViewModel> Handle(GetTodoListEntityQuery request, CancellationToken cancellationToken)
            {
                TodoListEntityViewModel viewModel = new TodoListEntityViewModel();

                viewModel.TodoList = await _context.TodoListEntities
                    .ProjectTo<TodoListEntityDto>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(dto => dto.Id == request.Id, cancellationToken);
                
                if (viewModel.TodoList == null)
                {
                    throw new NotFoundException(nameof(viewModel.TodoList), request.Id);
                }
                return viewModel;
            }
        }
    }
}
