﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.TodoListItemEntities.Commands.DeleteTodoListItemEntity
{
    public class DeleteTodoListItemEntityCommand : IRequest
    {
        public int ItemId { get; set; }
        
        public int ListId { get; set; }
        
        public class DeleteTodoListItemEntityCommandHandler : IRequestHandler<DeleteTodoListItemEntityCommand>
        {
            private readonly IApplicationDbContext _context;

            public DeleteTodoListItemEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }
            public async Task<Unit> Handle(DeleteTodoListItemEntityCommand request, CancellationToken cancellationToken)
            {
                TodoListEntity todoListEntity = await _context.TodoListEntities.FindAsync(request.ListId);
                if (todoListEntity == null)
                {
                    throw new NotFoundException(nameof(TodoListEntity), request.ListId);
                }

                var todoListItemEntity = await _context.TodoListItemEntities.FindAsync(request.ItemId);
                if (todoListItemEntity == null)
                {
                    throw new NotFoundException(nameof(todoListItemEntity), request.ItemId);
                }
                
                if(todoListItemEntity.Completed == true){
                    todoListItemEntity.DeletedAt = new DateTimeOffset(DateTime.Now);
                    _context.TodoListItemEntities.Update(todoListItemEntity);
                }

                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
