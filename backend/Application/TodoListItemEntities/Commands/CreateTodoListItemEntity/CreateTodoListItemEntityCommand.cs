﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.TodoListItemEntities.Commands.CreateTodoListItemEntity
{
    public class CreateTodoListItemEntityCommand : IRequest<int>
    {
        public int ListId { get; set; }
        public string ItemName { get; set; }

        public class CreateTodoListItemEntityCommandHandler : IRequestHandler<CreateTodoListItemEntityCommand, int>
        {
            private readonly IApplicationDbContext _context;

            public CreateTodoListItemEntityCommandHandler (IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<int> Handle(CreateTodoListItemEntityCommand request, CancellationToken cancellationToken)
            {
                TodoListEntity listEntity = await _context.TodoListEntities.FindAsync(request.ListId);
                if (listEntity == null)
                {
                    throw  new NotFoundException(nameof(TodoListEntity), request.ListId);

                }

                TodoListItemEntity todoListEntity = new TodoListItemEntity()
                {
                    ItemName = request.ItemName, 
                    ParentTodoListId = request.ListId
                };

                _context.TodoListItemEntities.Add(todoListEntity);

                await _context.SaveChangesAsync(cancellationToken);
                
                return todoListEntity.Id;
            }
        }
    }
}
