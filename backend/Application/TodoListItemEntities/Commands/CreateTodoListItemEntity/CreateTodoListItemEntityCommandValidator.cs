﻿using System.Data;
using FluentValidation;

namespace Application.TodoListItemEntities.Commands.CreateTodoListItemEntity
{
    public class CreateTodoListItemEntityCommandValidator : AbstractValidator<CreateTodoListItemEntityCommand>
    {
        public CreateTodoListItemEntityCommandValidator()
        {
            RuleFor(e => e.ListId)
                .NotEmpty();
            RuleFor(e => e.ItemName)
                .MaximumLength(25)
                .NotEmpty();
        }
    }
}
