﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;

namespace Application.TodoListItemEntities.Commands.UpdateTodoListItemEntity
{
    public class UpdateTodoListItemEntityCommand : IRequest
    {

        public int ListId { get; set; }
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        
        public bool Completed { get; set; }


        public class UpdateTodoListEntityCommandHandler : IRequestHandler<UpdateTodoListItemEntityCommand>
        {
            private readonly IApplicationDbContext _context;

            public UpdateTodoListEntityCommandHandler(IApplicationDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(UpdateTodoListItemEntityCommand request, CancellationToken cancellationToken)
            {
                TodoListEntity todoListEntity = await _context.TodoListEntities.FindAsync(request.ListId);

                if (todoListEntity == null)
                {
                    throw new NotFoundException(nameof(todoListEntity), request.ListId);
                }

                var todoListItemEntity = await _context.TodoListItemEntities.FindAsync(request.ItemId);

               if (todoListItemEntity == null)
               {
                   throw  new NotFoundException(nameof(TodoListItemEntity), request.ItemId);
               }
               
               todoListItemEntity.ItemName = request.ItemName;
               todoListItemEntity.Completed = request.Completed;
               _context.TodoListItemEntities.Update(todoListItemEntity);
               await _context.SaveChangesAsync(cancellationToken);

               return Unit.Value;
            }
        }
    }
}
