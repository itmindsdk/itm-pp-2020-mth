﻿using Application.TodoListEntities.Commands.UpdateTodoListEntity;
using FluentValidation;

namespace Application.TodoListItemEntities.Commands.UpdateTodoListItemEntity
{
    public class UpdateTodoListItemEntityCommandValidator : AbstractValidator<UpdateTodoListEntityCommand>
    {

        public UpdateTodoListItemEntityCommandValidator()
        {
            RuleFor(e => e.Name)
                .MaximumLength(25)
                .NotEmpty();
        }
    }
}
