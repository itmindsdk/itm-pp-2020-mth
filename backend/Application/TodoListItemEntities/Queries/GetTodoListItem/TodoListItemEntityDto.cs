﻿using System.Collections.Generic;
using Application.Common.Mappings;
using Application.TodoListItemEntities.Queries.GetTodoListsWithItemsEntities;
using AutoMapper;
using Domain.Entities;

namespace Application.TodoListItemEntities.Queries.GetTodoListItem
{
    public class TodoListItemEntityDto : IMapFrom<TodoListItemEntity>
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        
        public bool Completed { get; set; }
        
        public void Mapping(Profile profile)
        {
            profile.CreateMap<TodoListItemEntity, TodoListItemEntityDto>()
                .ForMember(d => d.ItemName, opt => opt.MapFrom(s => s.ItemName))
                .ForMember(d => d.Completed, opt => opt.MapFrom(s => s.Completed))
                .ForMember(d => d.ItemId, opt => opt.MapFrom(s => s.Id));
            
        }
    }
}