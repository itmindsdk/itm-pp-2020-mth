﻿using System.Collections.Generic;
using Application.TodoListItemEntities.Queries.Common;

namespace Application.TodoListItemEntities.Queries.GetTodoListItem
{
    public class TodoListItemEntityViewModel
    {
        public TodoListItemEntityDto ListItem { get; set; }

    }
}
