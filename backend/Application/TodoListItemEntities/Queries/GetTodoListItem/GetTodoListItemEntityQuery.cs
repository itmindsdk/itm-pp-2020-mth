﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.TodoListItemEntities.Queries.Common;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListItemEntities.Queries.GetTodoListItem
{
    public class GetTodoListItemEntityQuery : IRequest<TodoListItemEntityViewModel>
    {
        public int ListId { get; set; }
        public int ItemId { get; set; }

        public class GetTodoListEntityQueryHandler : IRequestHandler<GetTodoListItemEntityQuery, TodoListItemEntityViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodoListEntityQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<TodoListItemEntityViewModel> Handle(GetTodoListItemEntityQuery request, CancellationToken cancellationToken)
            {
                TodoListItemEntityViewModel viewModel = new TodoListItemEntityViewModel();

                viewModel.ListItem = await _context.TodoListItemEntities
                    .Where(item => item.DeletedAt == null)
                    .ProjectTo<TodoListItemEntityDto>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync((dto => dto.ItemId == request.ItemId),cancellationToken);
                if (viewModel.ListItem == null)
                {
                    throw new NotFoundException(nameof(viewModel.ListItem), request.ItemId);

                }

                return viewModel;
            }
        }
    }
}
