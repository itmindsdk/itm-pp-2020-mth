﻿using System.Collections.Generic;
using System.Linq;
using Application.Common.Mappings;
using Application.TodoListItemEntities.Queries.GetTodoListItem;
using AutoMapper;
using Domain.Entities;

namespace Application.TodoListItemEntities.Queries.Common
{
    public class TodoListWithItemsEntityDto : IMapFrom<TodoListEntity>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        
        public IList<TodoListItemEntityDto> ListItems { get; set; }
        public void Mapping(Profile profile)
        {
            profile.CreateMap<TodoListEntity, TodoListWithItemsEntityDto>()
                .ForMember(d => d.Name, opt => opt.MapFrom(s => s.ListName))
                .ForMember(d => d.ListItems, opt => opt.MapFrom(s => s.ListItems.Where(item => item.DeletedAt == null)))
                .ForMember(d => d.Id, opt => opt.MapFrom( s=>s.Id));
            
            
        }
    }
}
