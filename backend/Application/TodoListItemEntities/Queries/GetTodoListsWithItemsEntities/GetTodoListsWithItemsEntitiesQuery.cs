﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.TodoListEntities.Queries.Common;
using Application.TodoListEntities.Queries.GetTodoListEntities;
using Application.TodoListItemEntities.Queries.Common;
using Application.TodoListItemEntities.Queries.GetTodoListItem;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListItemEntities.Queries.GetTodoListsWithItemsEntities
{
    public class GetTodoListsWithItemsEntitiesQuery : IRequest<TodoListsWithItemsEntitiesViewModel>
    {

        public class GetTodoListEntityQueryHandler : IRequestHandler<GetTodoListsWithItemsEntitiesQuery, TodoListsWithItemsEntitiesViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;
            public GetTodoListEntityQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;




            }
            public async Task<TodoListsWithItemsEntitiesViewModel> Handle(GetTodoListsWithItemsEntitiesQuery request, CancellationToken cancellationToken)
            {
                
                /** Mapping and filtering manually
                 * .Include(list => list.ListItems)
                    .Select(list => new TodoListWithItemsEntityDto
                    {
                        Id = list.Id,
                        Name = list.ListName,
                        ListItems = list.ListItems.Where(item => item.DeletedAt == null).Select(item => new TodoListItemEntityDto
                        {
                            ItemId = item.Id,
                            ItemName = item.ItemName,
                            Completed = item.Completed
                        }).ToList()
                    })
                 */
                TodoListsWithItemsEntitiesViewModel viewModel = new TodoListsWithItemsEntitiesViewModel();
                viewModel.TodoLists = await _context.TodoListEntities
                    .ProjectTo<TodoListWithItemsEntityDto>(_mapper.ConfigurationProvider)
                    .ToListAsync(cancellationToken);
                
                return viewModel;
            }
        }
    }
}
