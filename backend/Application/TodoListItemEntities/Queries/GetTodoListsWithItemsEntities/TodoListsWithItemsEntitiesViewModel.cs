﻿using System.Collections.Generic;
using Application.TodoListItemEntities.Queries.Common;

namespace Application.TodoListItemEntities.Queries.GetTodoListsWithItemsEntities
{
    public class TodoListsWithItemsEntitiesViewModel
    {
        public IList<TodoListWithItemsEntityDto> TodoLists { get; set; }

    }
}
