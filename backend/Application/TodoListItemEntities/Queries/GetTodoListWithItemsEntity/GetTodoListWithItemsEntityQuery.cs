﻿using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.TodoListItemEntities.Queries.Common;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.TodoListItemEntities.Queries.GetTodoListWithItemsEntity
{
    public class GetTodoListWithItemsEntityQuery : IRequest<TodoListWithItemsEntityViewModel>
    {
        public int ListId { get; set; }
        public class GetTodoListWithItemsEntitiesQueryHandler : IRequestHandler<GetTodoListWithItemsEntityQuery, TodoListWithItemsEntityViewModel>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodoListWithItemsEntitiesQueryHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }
            public async Task<TodoListWithItemsEntityViewModel> Handle(GetTodoListWithItemsEntityQuery request, CancellationToken cancellationToken)
            {
                var viewModel = new TodoListWithItemsEntityViewModel();
                viewModel.TodoList = await _context.TodoListItemEntities
                    .ProjectTo<TodoListWithItemsEntityDto>(_mapper.ConfigurationProvider)
                        .FirstOrDefaultAsync((dto => dto.Id == request.ListId),cancellationToken);
                
                if (viewModel.TodoList == null)
                {
                    throw new NotFoundException(nameof(viewModel.TodoList), request.ListId);
                }
                return viewModel;
            }
        }
    }
}
