﻿using Application.TodoListItemEntities.Queries.Common;
using Application.TodoListItemEntities.Queries.GetTodoListsWithItemsEntities;

namespace Application.TodoListItemEntities.Queries.GetTodoListWithItemsEntity
{
    public class TodoListWithItemsEntityViewModel
    {
        public TodoListWithItemsEntityDto TodoList { get; set; }

    }
}
