﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;

namespace Application.ExampleEntities.Commands.CreateExampleEntity
{
    public class CreateExampleEntityCommandValidator : AbstractValidator<CreateExampleEntityCommand>
    {
        public CreateExampleEntityCommandValidator()
        {
            RuleFor(e => e.Name)
                .MaximumLength(25)
                .NotEmpty();
            RuleFor(e => e.ExampleEnum)
                .IsInEnum()
                .NotNull();
        }
    }
}
