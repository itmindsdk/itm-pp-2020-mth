﻿using System.Collections.Generic;
using Domain.Common;

namespace Domain.Entities
{
    public class TodoListEntity : AuditableEntity
    {
        public int Id { get; set; }
        public string ListName { get; set; }
        public ICollection<TodoListItemEntity> ListItems { get; set; }
    }
}