﻿using System;
using Domain.Common;

namespace Domain.Entities
{
    public class TodoListItemEntity : AuditableEntity
    {
        public int Id { get; set; }
        public bool Completed { get; set; }
        public int? ParentTodoListId { get; set; }
        public virtual TodoListEntity ParentTodoList { get; set; }
        public string ItemName { get; set; }

        public DateTimeOffset? DeletedAt { get; set; }
    }
}