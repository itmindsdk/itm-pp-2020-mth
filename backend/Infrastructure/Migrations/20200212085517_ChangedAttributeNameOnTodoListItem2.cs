﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class ChangedAttributeNameOnTodoListItem2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoListItemEntities_TodoListEntities_ParentTodoListEntityId",
                table: "TodoListItemEntities");

            migrationBuilder.DropIndex(
                name: "IX_TodoListItemEntities_ParentTodoListEntityId",
                table: "TodoListItemEntities");

            migrationBuilder.DropColumn(
                name: "ListId",
                table: "TodoListItemEntities");

            migrationBuilder.DropColumn(
                name: "ParentTodoListEntityId",
                table: "TodoListItemEntities");

            migrationBuilder.AddColumn<int>(
                name: "ParentTodoListId",
                table: "TodoListItemEntities",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TodoListItemEntities_ParentTodoListId",
                table: "TodoListItemEntities",
                column: "ParentTodoListId");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoListItemEntities_TodoListEntities_ParentTodoListId",
                table: "TodoListItemEntities",
                column: "ParentTodoListId",
                principalTable: "TodoListEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoListItemEntities_TodoListEntities_ParentTodoListId",
                table: "TodoListItemEntities");

            migrationBuilder.DropIndex(
                name: "IX_TodoListItemEntities_ParentTodoListId",
                table: "TodoListItemEntities");

            migrationBuilder.DropColumn(
                name: "ParentTodoListId",
                table: "TodoListItemEntities");

            migrationBuilder.AddColumn<int>(
                name: "ListId",
                table: "TodoListItemEntities",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ParentTodoListEntityId",
                table: "TodoListItemEntities",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TodoListItemEntities_ParentTodoListEntityId",
                table: "TodoListItemEntities",
                column: "ParentTodoListEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoListItemEntities_TodoListEntities_ParentTodoListEntityId",
                table: "TodoListItemEntities",
                column: "ParentTodoListEntityId",
                principalTable: "TodoListEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
