﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class ChangedAttributeNameOnTodoListItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoListItemEntities_TodoListEntities_ListId",
                table: "TodoListItemEntities");

            migrationBuilder.DropIndex(
                name: "IX_TodoListItemEntities_ListId",
                table: "TodoListItemEntities");

            migrationBuilder.AddColumn<int>(
                name: "ParentTodoListEntityId",
                table: "TodoListItemEntities",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TodoListItemEntities_ParentTodoListEntityId",
                table: "TodoListItemEntities",
                column: "ParentTodoListEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoListItemEntities_TodoListEntities_ParentTodoListEntityId",
                table: "TodoListItemEntities",
                column: "ParentTodoListEntityId",
                principalTable: "TodoListEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TodoListItemEntities_TodoListEntities_ParentTodoListEntityId",
                table: "TodoListItemEntities");

            migrationBuilder.DropIndex(
                name: "IX_TodoListItemEntities_ParentTodoListEntityId",
                table: "TodoListItemEntities");

            migrationBuilder.DropColumn(
                name: "ParentTodoListEntityId",
                table: "TodoListItemEntities");

            migrationBuilder.CreateIndex(
                name: "IX_TodoListItemEntities_ListId",
                table: "TodoListItemEntities",
                column: "ListId");

            migrationBuilder.AddForeignKey(
                name: "FK_TodoListItemEntities_TodoListEntities_ListId",
                table: "TodoListItemEntities",
                column: "ListId",
                principalTable: "TodoListEntities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
