﻿using Application.Common.Interfaces;
using Domain.Entities;
using Infrastructure.Persistence;
using Moq;
using System;
using System.Threading.Tasks;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Shouldly;
using Xunit;

namespace Infrastructure.IntegrationTests.Persistence
{
    public class ApplicationDbContextTests : IDisposable
    {
        private readonly string _userId;
        private readonly DateTimeOffset _dateTimeOffset;
        private readonly Mock<IDateTimeOffset> _dateTimeOffsetMock;
        private readonly Mock<ICurrentUserService> _currentUserServiceMock;
        private readonly ApplicationDbContext _context;

        public ApplicationDbContextTests()
        {
            _dateTimeOffset = new DateTimeOffset(3001, 1, 1, 1, 1, 1,TimeSpan.Zero);
            _dateTimeOffsetMock = new Mock<IDateTimeOffset>();
            _dateTimeOffsetMock.Setup(m => m.Now).Returns(_dateTimeOffset);

            _userId = "00000000-0000-0000-0000-000000000000";
            _currentUserServiceMock = new Mock<ICurrentUserService>();
            _currentUserServiceMock.Setup(m => m.UserId).Returns(_userId);

            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            _context = new ApplicationDbContext(options, _currentUserServiceMock.Object, _dateTimeOffsetMock.Object);

            _context.ExampleEntities.Add(new ExampleEntity
            {
                Id = 1,
                Name = "Test",
                ExampleEnum = ExampleEnum.A
            });

            _context.SaveChanges();
        }

        [Fact]
        public async Task SaveChangesAsync_GivenNewTodoItem_ShouldSetCreatedProperties()
        {
            var item = new ExampleEntity
            {
                Id = 2,
                Name = "Test2",
                ExampleEnum = ExampleEnum.A
            };

            _context.ExampleEntities.Add(item);

            await _context.SaveChangesAsync();

            item.Created.ShouldBe(_dateTimeOffset);
            item.CreatedBy.ShouldBe(_userId);
        }

        [Fact]
        public async Task SaveChangesAsync_GivenExistingTodoItem_ShouldSetLastModifiedProperties()
        {
            int id = 1;

            var item = await _context.ExampleEntities.FindAsync(id);

            item.ExampleEnum = ExampleEnum.D;

            await _context.SaveChangesAsync();

            item.LastModified.ShouldNotBeNull();
            item.LastModified.ShouldBe(_dateTimeOffset);
            item.LastModifiedBy.ShouldBe(_userId);
        }
        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}
